// Viene de: https://0xacab.org/utopias-piratas/jekyll-streaming/-/blob/master/assets/streaming.js

// Estamos esperando la descarga de un stream
var waiting = false;
// El intervalo que reintenta, si está vacío se crea uno nuevo al
// siguiente error.
var waiter  = null;

function streaming() {
  // Si ya estamos esperando no tenemos que hacer nada
  if (waiting) return;

  // Indicarle a otros streamers que ya estamos trabajando.
  waiting = true;

  // Encontrar todos los <video>
  var video = $('video');
  // Cambiar el src para que incluya un timestamp, esto engaña a Firefox
  // a recargar el video cuando se corta en lugar de pensar que terminó
  // la descarga y empezar a reproducir desde el principio.
  video.find('source').attr('src', function() {
    var source = $(this);
    // Obtener la URL completa hasta el ?
    var src = source.attr('src').split('?')[0];
    return src+'?'+Date.now().toString();
  });

  // Empezar la reproducción
  video.get(0).load();
  video.get(0).play();
}

$(document).ready(function() {
  // Iniciar el streaming al cargar la página
  streaming();

  // Cuando estamos reproduciendo dejamos de esperar y limpiamos todas
  // las recargas después de un error
  $('video').on('playing', function() {
    waiting = false;
    if (waiter) {
      clearInterval(waiter);
      waiter = null;
    }
  });

  // Volver a reproducir cuando se corte
  $('video').on('ended', function() {
    console.log('Reiniciando transmisión');
    streaming();
  });

  // Si falla la carga, por ejemplo mientras vuelve la transmisión,
  // reintentar regularmente.  Si ya estamos intentando, no hacer nada.
  $('video source').on('error', function() {
    // Señalamos que el intento anterior falló
    waiting = false;
    // Si no hay una tarea esperando, configurarla
    if (!waiter) {
      console.log('Falló la carga de la fuente, reiniciando transmisión.');
      // Reintentar cada 2 segundos.
      waiter = setInterval(streaming, 2000);
    }
  });
});
