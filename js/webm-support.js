// Viene de: https://stackoverflow.com/questions/7451635/how-to-detect-supported-video-formats-for-the-html5-video-tag
function webm_support () {
  var test = document.createElement('video'),
      webm, err, msg;

  if (test.canPlayType) {
    webm = "" !== test.canPlayType('video/webm; codecs="vp8, vorbis"');
  }

  if (!webm) {
    err = document.createElement('div');
    msg = document.createElement('p');

    err.id = 'error'
    msg.innerHTML = 'ERROR: explorador no soportado. <br/> Por favor prueba con Firefox, Chromium o Chrome.';
    err.appendChild(msg);
    document.body.appendChild(err);
  }
}
